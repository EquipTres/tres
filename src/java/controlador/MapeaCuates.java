package controlador;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

@ManagedBean(name="buscaCuates",eager=true)
@ApplicationScoped
public class MapeaCuates implements CuateBuscaInterface
{
    private Collection <Cuate> lista= new ArrayList<Cuate>();
    private Map<String,Cuate > cuates;
    public MapeaCuates()
    {
        cuates=new HashMap<>();
         addCuate(new Cuate ("user1", "123", "Pérez Pérez", "Pedro", 2345.60,"cara1.png"));
        addCuate(new Cuate ("user2", "abc", "Lopez Moreno", "Eduardo", 50.00,"cara3.jpg"));
        addCuate(new Cuate ("user3", "abc123", "Nava Rosales", "Omar", 68.00,"cara2.jpg"));
    }

    @Override
    public Cuate BuscaCuate(String user, String pwd) 
    {
        Cuate cuate=null;
        System.out.println(user);
        if (user!=null)
        {
            cuate=cuates.get(user.toLowerCase());
            if (cuate!=null)
            {
                if (cuate.getPwd().equals(pwd))
                {
                    System.out.println("ES IGUAL EL PASSWORD");
                    return cuate;
                }
                else
                    return (null);
            }
        }
        return cuate;
    }
    
    private void addCuate(Cuate cuate)
    {
        System.out.println("CUOTAS:"+ cuate.getCuotas());
        cuates.put(cuate.getUser(), cuate);
        lista.add(cuate); 
    }

    @Override
    public Collection<Cuate> ListaCuate() 
    {
        
        return lista;
    }
    
}
